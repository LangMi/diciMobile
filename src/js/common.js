/*****************接口管理*********************/
var website = "http://192.168.1.246:9090/diciDetectTool/";
var interface ={
  "cmdList":website+"mobile/cmdList",//指令列表
  "recvDataList":website+"mobile/recvDataList",//历史接收数据列表
  "diciList":website+"mobile/diciList",// 地磁列表
  "pspList":website+"mobile/pspList",//停车位列表
  "parkList":website+"mobile/parkList",//停车场列表
  "staffList":website+"mobile/staffList",//无车标尺
  "login":website+"mobile/login",//登录
  "diciOne":website+"mobile/findOne",//地磁详情
  "resetNoCar":website+"mobile/resetNoCar",//重置无车
  "sendCmd":website+"mobile/sendCmd",//发送指令
  "sendParam":website+"mobile/sendParam",//下发系统参数
  "preSendDCparam":website+"mobile/preSendDCparam",//下发系统参数前调用
  "paramList":website+"mobile/paramList",//系统参数列表
  "staffList":website+"mobile/staffList",//标尺列表
  "signalList":website+"mobile/recvSignalDataList",//信号质量列表
  "logout":website+"mobile/logout"
  
}
var dialog = window.YDUI.dialog;

// $(window).scroll(function () {
//   if($(window).scrollTop()>$(window).height()){
//     $("#toTop").show(0);
//   }else{
//     $("#toTop").hide(0);
//   }
// });
/*****************退出事件*********************/
var logout=function(){
  YDUI.util.cookie.set('username',"",0);
  localStorage.removeItem('paramObj');
  localStorage.removeItem('paramItem');
  localStorage.removeItem('historyListId');
  window.location=location.origin+"/diciMobile/login.html";
}

/*****************头部初始化*********************/
var navbarInit=function(){

  /*****************返回事件*********************/
  $("#back").on('click',function(e){
    e.preventDefault();
    history.back();
  });
  /*****************菜单出现*********************/
  var $userBox=$("#userBox");
  $("#userItem").on('click',function(e){
    e.stopPropagation();
      $userBox.slideDown(200);
      $(".user .next-ico").addClass("up");
  });
  $(document).bind('click',function(e){ 
    var e = e || window.event; 
    var elem = e.target || e.srcElement; 
    while (elem) { 
    if (elem.id && elem.id=='userItem') { 
    return; 
    } 
    elem = elem.parentNode; 
    } 
    $userBox.slideUp(50);
    $(".user .next-ico").removeClass("up"); 
  }); 

  /*****************退出按钮*********************/
  $("#btnQuit").off('click').on('click',function(){
    // localStorage.removeItem('username');
    $.ajax({
      type:"post",
      url: interface.logout,
      dataType: 'json',
      xhrFields: {withCredentials: true},
      success: function () {
        logout();
      },
      error:function(data){
          console.log(data);
      }
    });
  });
  /*****************缓存登录*********************/
  // var localName = localStorage.getItem('username');
  var localName = YDUI.util.cookie.get('username');
  if(!localName){
    window.location=location.origin+"/diciMobile/login.html";
  }
  if($('.username').length){
    $('.username').html(localName);
  } 

}

/*****************输入框取消*********************/ 
$(".input-cancel").each(function(){
  if(($(this).attr("disabled")==undefined)&&($(this).val()!="")){
    $(this).after('<i class="input-cancel-icon icon-error"></i>');
  }
});
$("body").on("keyup",".input-cancel",function(){
  if($(this).val()==""){
    $(this).next(".input-cancel-icon").remove();
  }else if($(this).next(".input-cancel-icon").length==0){
    $(this).after('<i class="input-cancel-icon icon-error"></i>');
  }
});
$("body").on("click",".input-cancel",function(){
  if($(this).val()==""){
    $(this).next(".input-cancel-icon").remove();
  }else if($(this).next(".input-cancel-icon").length==0){
    $(this).after('<i class="input-cancel-icon icon-error"></i>');
  }
});
$("body").on("click",".input-cancel-icon",function(){
  $(this).prev().val("");
  $(this).remove();
});

