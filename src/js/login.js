$("input").on("blur",function(){
    var $label=$(this).parent(".input-item").children("label");
    if($(this).val()!=""){
        $label.html("");
    }else{
        if($(this).attr("id")=="username"){
            $label.html("用户名");
        }else{
            $label.html("密码");
        }
    }
});
$('#btn_login').click(function(){
    var username = $('input[name="username"]').val(),
        password = $('input[name="password"]').val();
    if(!username){
        dialog.toast('用户名不能为空', 'none', 1500);
        return ;
    }
    if(!password){
        dialog.toast('密码不能为空', 'none', 1500);
        return ;
    }
    $.ajax({
        type:"post",
        url: interface.login,
        dataType: 'json',
        xhrFields: {withCredentials: true},
        data: {
            "name": username,
            "password": password
        },
        success: function (data) {
            if(!data.success){
                dialog.toast(data.msg, 'none', 1500);
                return ;
            }
            YDUI.util.cookie.set('username',username,3600);
            // localStorage.setItem('username',username);
            window.location=location.origin+"/diciMobile/index.html";
        },
        error:function(data){
            console.log(data);
        }
    });
})