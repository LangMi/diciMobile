var commandListJs={
    param:{
        // "pageSize": '',
        // "pageNo": '',
        "nodeId": "",
        "diciNo": "",
        "startTime": "",
        "endTime": ""
    },
    init:function(nodeId){
        this.param.nodeId=(nodeId!=undefined) ? nodeId : "";
        this.render("",this.param.parkName,"","");
    },
    render:function(pspId,parkName,nodeId,nowState){
        $('#commandList').html('<div class="listcontent" id="commandListContent"></div>');
        var pageNo = 0, pageSize = 20;
        $("#YDUI_CONFRIM").remove();
        $('#commandList').infiniteScroll({
            pageSize: pageSize,
            initLoad: true,
            loadingHtml: '<strong>加载中...</strong>',
            doneTxt: '没有更多数据了',
            loadListFn: function () {
                var def = $.Deferred();
                var defaultsData = $.extend(true,commandListJs.param,{
                    // "pageNo": pageNo,
                    // "pageSize":  pageSize
                });               
                $.ajax({
                    type:"post",
                    url: interface.cmdList,
                    dataType: 'json',
                    data: defaultsData,
                    xhrFields: {withCredentials: true},
                    success: function (data) {
                        if(data.code==500){
                            dialog.alert(data.msg,function(){
                                logout();
                            });
                        }
                        if(data.length!=0){
                            var listHtml = '';
                            for(var i=0;i<data.length;i++){
                                var obj=data[i];
                                var statusClass=(obj.executeStatus=="SUCCESS")?"cmd-status-green":"cmd-status-red";
                                listHtml += '<div class="cmd-item">'+
                                    '<div class="cmd-info">'+
                                        '<div class="cmd-name">'+obj.commandCode+'</div>'+
                                        '<div class="cmd-status '+statusClass+'">'+obj.executeStatus+'</div>'+
                                    '</div>'+
                                    '<div class="cmd-time">'+
                                        '<span class="uptime"><i class="icon-arrow-up"></i>'+obj.sendTime+'</span>'+
                                        '<span class="downtime"><i class="icon-arrow-down"></i>'+obj.updateTime+'</span>'+
                                    '</div>'+
                                '</div>';                               
                            }
                            $('#commandListContent').append(listHtml);
                            def.resolve(data);
                            ++pageNo;
                        }else{
                            $('.list-loading').hide();
                            $('#commandList').append('<div class="list-donetip">没有更多数据了</div>');
                        }                     
                    },
                    error:function(){
                        $('#commandList').html('<div class="list-donetip">未知错误</div>');
                    }
                });
                return def.promise();
            }
        });
    }
};