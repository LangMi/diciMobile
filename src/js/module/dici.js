var diciJs={
    param:{
        pspId:"",
        parkName:"",
        nodeId:"",
        onlineFlag:"",
        diciNo:"",
        diciStatus:"",
        bindFlag:""
    },
    init:function(){
        this.filter();
        this.render();
    },
    render:function(pspId,parkName,nodeId,onlineFlag,diciNo,diciStatus,bindFlag){
        var diciNoInput =$("#YDUI_CONFRIM input[name='diciNo']").val()||"";
        if(diciNoInput==""||/^\+?[1-9][0-9]*$/.test(diciNoInput)){
        }else{
            dialog.alert('地磁编号必须为整数');
            return;
        }
        var listHeight=$('#diciList').height();
        $('#diciList').remove();
        $(".dici-item").append('<div class="list" id="diciList" style="height:'+listHeight+'px">'+
            '<div class="listcontent" id="diciContent">'+
            '<div class="top" id="dicitop"></div>'+
            '</div>'+
        '</div>');
        // $('#diciList').html('<div class="listcontent" id="diciContent"></div>');
        var pageNo = 0, pageSize = 20;
        $("#YDUI_CONFRIM .confirm-ft a").each(function(){
            if($(this).text()=="取消"){
                $(this).click();
            }
        });
        $('#diciList').infiniteScroll({
            pageSize: pageSize,
            initLoad: true,
            binder:$("#diciList"),
            loadingHtml: '<strong>加载中...</strong>',
            loadListFn: function () {
                var def = $.Deferred();
                $.ajax({
                    type:"post",
                    url: interface.diciList,
                    dataType: 'json',
                    xhrFields: {withCredentials: true},
                    data: {pageNo: pageNo, pageSize: pageSize,parkName:parkName,pspId:pspId,nodeId:nodeId,onlineFlag:onlineFlag,diciNo:diciNo,diciStatus:diciStatus,bindFlag:bindFlag},
                    success: function (data) {
                        if(data.code==500){
                            dialog.alert(data.msg,function(){
                                logout();
                            });
                        }
                        if(data.length!=0){
                            for(var i=0;i<data.length;i++){
                                var obj=data[i];
                                var circle=(obj.diciStatus==1)? "circle-orange":"circle-green";
                                var isOnline=(obj.onlineFlag==true)? "在线":"离线";
                                var parkname=(obj.parkName!=null&&obj.parkName!="")? obj.parkName:"无停车场";
                                var html='<div class="cell-item" data-nodeid="'+obj.nodeId+'"><div class="cell-left"><div><span>['+isOnline+']</span>'+obj.diciNo+'<span class="addr">/'+parkname+'</span><div class="lineTwo">IMEI号：'+obj.nodeId+'</div></div></div><div class="cell-right cell-arrow"><span class="'+circle+'"></span></div></div>';
                                $('#diciContent').append(html);
                            }
                            def.resolve(data);
                            ++pageNo;
                        }else{
                            $('.list-loading').hide();
                            $('#diciList').append('<div class="list-donetip">没有更多数据了</div>');
                        }
                        
                    },
                    error:function(){
                        $('#diciList').html('<div class="list-donetip">未知错误</div>');
                    }
                });
                return def.promise();
            }
        });
        this.itemClickFunc();
    },
    itemClickFunc:function(){
        $("#diciContent").on('click',".cell-item",function(){
            var nodeId=$(this).data("nodeid");
            location.href=location.origin+"/diciMobile/views/diciDetail.html?nodeId="+nodeId;
        });
    },
    filter:function(){
        var _this=this;
        $("#chooseDici").on("click",function(){
            dialog.confirm('地磁筛选', $("#dialogContent").find(".diciDialog").html()
            .replace(/"pspId"/,'"pspId"'+' value='+_this.param.pspId+' ')
            .replace(/"parkName"/,'"parkName"'+' value='+_this.param.parkName+' ')
            .replace(/"nodeId"/,'"nodeId"'+' value='+_this.param.nodeId+' ')
            .replace(/"diciNo"/,'"diciNo"'+' value='+_this.param.diciNo+' ')
            .replace(/"diciStatus"/,'"diciStatus"'+' value='+_this.param.diciStatus+' ')
            .replace(/"bindFlag"/,'"bindFlag"'+' value='+_this.param.bindFlag+' ')
            .replace(/"onlineFlag"/,'"onlineFlag"'+' value='+_this.param.onlineFlag+' ')
            .replace(/{value="0|1" selected}/,"$1+=false")
            .replace('"onlineFlag" value="'+_this.param.onlineFlag+'"','"onlineFlag" value="'+_this.param.onlineFlag+'" selected')
            .replace('"bindFlag" value="'+_this.param.bindFlag+'"','"bindFlag" value="'+_this.param.bindFlag+'" selected')
            .replace('"diciStatus" value="'+_this.param.diciStatus+'"','"diciStatus" value="'+_this.param.diciStatus+'" selected'), [
                {
                    txt: '取消',
                    color: false, /* false:黑色  true:绿色 或 使用颜色值 */
                    callback: function () {
                    }
                },
                {
                    txt: '重置',
                    stay: true, /* 是否保留提示框 */
                    color: false, /* 使用颜色值 */
                    callback: function () {
                        $("#YDUI_CONFRIM").find("input").val("");
                        $("#YDUI_CONFRIM").find("select").val("");
                    }
                },
                {
                    txt: '确定',
                    color: true,
                    stay: true,
                    callback: function () {
                        var pspId = $("#YDUI_CONFRIM").find("input[name='pspId']").val();
                        var parkName = $("#YDUI_CONFRIM").find("input[name='parkName']").val();
                        var nodeId = $("#YDUI_CONFRIM").find("input[name='nodeId']").val();
                        var diciNo = $("#YDUI_CONFRIM").find("input[name='diciNo']").val();
                        var diciStatus = $("#YDUI_CONFRIM").find("select[name='diciStatus']").val();
                        var onlineFlag = $("#YDUI_CONFRIM").find("select[name='onlineFlag']").val();
                        var bindFlag = $("#YDUI_CONFRIM").find("select[name='bindFlag']").val();
                        _this.param.pspId=pspId;
                        _this.param.parkName=parkName;
                        _this.param.nodeId=nodeId;
                        _this.param.onlineFlag=onlineFlag;
                        _this.param.diciNo=diciNo;
                        _this.param.diciStatus=diciStatus;
                        _this.param.bindFlag=bindFlag;
                        _this.render(pspId,parkName,nodeId,onlineFlag,diciNo,diciStatus,bindFlag);
                    }
                }
            ]);
        });
    }
};