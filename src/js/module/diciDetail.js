 var nodeId=YDUI.util.getQueryString("nodeId");
 console.log(nodeId)
 /*****************基本信息*********************/
 var obj;
$(function () {
    $.ajax({
        type:"post",
        async: false,
        url: interface.diciOne,
        dataType: 'json',
        data: {nodeId:nodeId},
        xhrFields: {withCredentials: true},
        success: function (data) {
            if(data.code==500){
                dialog.alert(data.msg,function(){
                    logout();
                });
            }
            obj=data;
            $(".dtl-box .dtl-item").find(".dtl-right").each(function(idx,elem){
                var param=$(elem).attr("data-param");
                if(param=="diciStatus"){
                    $(elem).text(data[param]==0?"无车":"有车");
                }else if(param=="onlineFlag"){
                    $(elem).text(data[param]==true?"已入网":"未入网");
                }else{
                    $(elem).text(data[param]);
                }
            });
        },
        error:function(data){
            console.log(data)
        }
    });
});
  /*****************重置无车*********************/
$("#resetNoCar").on("click",function(){
    var data = {
        data : [{"IMEI":nodeId}]
    };
    data = JSON.stringify(data);
    dialog.confirm("",'确定重置无车吗?', function () {
        $.ajax({
            type:"post",
            url: interface.resetNoCar,
            dataType: 'json',
            data: data,
            xhrFields: {withCredentials: true},
            contentType : "application/json",
            beforeSend:function(){
                dialog.loading.open('重置中');
            },
            success: function (data) {
                if(data.code==500){
                    dialog.alert(data.msg,function(){
                        logout();
                    });
                }
                var obj=data[0];
                dialog.loading.close();
                if(obj.error_code==0){
                    dialog.toast('重置成功！', 'success', 1000);
                }else{
                    dialog.toast(obj.error_info, 'error', 1000);
                }
            }
        });
    });
});
  /*****************重置绝对标尺*********************/
$("#resetAbsScale").on("click",function(){
    var data = {
        data : [{"IMEI":nodeId}],
        commandCode : "resetAbsScale"
    };
    data = JSON.stringify(data);
    dialog.confirm("",'确定重置绝对标尺吗?', function () {
        $.ajax({
            type:"post",
            url: interface.sendCmd,
            dataType: 'json',
            data: data,
            xhrFields: {withCredentials: true},
            contentType : "application/json",
            beforeSend:function(){
                dialog.loading.open('重置中');
            },
            success: function (data) {
                if(data.code==500){
                    dialog.alert(data.msg,function(){
                        logout();
                    });
                }
                var obj=data[0];
                dialog.loading.close();
                if(obj.error_code==0){
                    dialog.toast('重置成功！', 'success', 1000);
                }else{
                    dialog.toast(obj.error_info, 'error', 1000);
                }
            }
        });
    });
});
  /*****************强制重启*********************/
$("#restart").on("click",function(){
    var data = {
        data : [{"IMEI":nodeId}],
        commandCode : "restart"
    };
    data = JSON.stringify(data);
    dialog.confirm("",'确定要强制重启吗?', function () {
        $.ajax({
            type:"post",
            url: interface.sendCmd,
            dataType: 'json',
            data: data,
            xhrFields: {withCredentials: true},
            contentType : "application/json",
            beforeSend:function(){
                dialog.loading.open('重启中');
            },
            success: function (data) {
                if(data.code==500){
                    dialog.alert(data.msg,function(){
                        logout();
                    });
                }
                var obj=data[0];
                dialog.loading.close();
                if(obj.error_code==0){
                    dialog.toast('重启成功！', 'success', 1000);
                }else{
                    dialog.toast(obj.error_info, 'error', 1000);
                }
            }
        });
    });
});
  /*****************下发系统参数*********************/
$("#sendParam").on("click",function(){
    var data = {
        data : [{"IMEI":nodeId}],
        commandCode : "restart"
    };
    data = JSON.stringify(data);
    dialog.confirm("",'确定要下发系统参数吗?', function () {
        $.ajax({
            type:"post",
            url: interface.preSendDCparam,
            dataType: 'json',
            xhrFields: {withCredentials: true},
            data: {diciNo:obj.diciNo,IMEI:obj.nodeId},
            beforeSend:function(){
                dialog.loading.open('请求中');
            },
            success: function (data) {
                if(data.code==500){
                    dialog.alert(data.msg,function(){
                        logout();
                    });
                }
                dialog.loading.close();
                if(data.success==false){
                    dialog.alert('请先执行查询系统参数操作！');
                }else{
                    var paramObj= data.object;
                    localStorage.setItem('paramObj', JSON.stringify(paramObj));
                    location.href=location.origin+"/diciMobile/views/param.html?nodeId="+obj.nodeId+"&type=edit";
                }
            }
        });
    });
    
}); 
  /*****************更新信号质量*********************/
$("#queryDCSign").on("click",function(){
    var data = {
        data : [{"IMEI":nodeId}],
        commandCode : "queryDCSign"
    };
    data = JSON.stringify(data);
    dialog.confirm("",'确定要查询信号质量吗?', function () {
        $.ajax({
            type:"post",
            url: interface.sendCmd,
            dataType: 'json',
            data: data,
            xhrFields: {withCredentials: true},
            contentType : "application/json",
            beforeSend:function(){
                dialog.loading.open('查询中');
            },
            success: function (data) {
                if(data.code==500){
                    dialog.alert(data.msg,function(){
                        logout();
                    });
                }
                var obj=data[0];
                dialog.loading.close();
                if(obj.error_code==0){
                    dialog.toast('查询成功，请去本地查看！', 'success', 1000);
                }else{
                    dialog.toast(obj.error_info, 'error', 1000);
                }
            }
        });
    });
    
});  
  /*****************更新系统参数*********************/
  $("#queryParam").on("click",function(){
    var data = {
        data : [{"IMEI":nodeId}],
        commandCode : "queryDCparam"
    };
    data = JSON.stringify(data);
    dialog.confirm("",'确定要查询系统参数吗?', function () {
        $.ajax({
            type:"post",
            url: interface.sendCmd,
            dataType: 'json',
            data: data,
            xhrFields: {withCredentials: true},
            contentType : "application/json",
            beforeSend:function(){
                dialog.loading.open('查询中');
            },
            success: function (data) {
                if(data.code==500){
                    dialog.alert(data.msg,function(){
                        logout();
                    });
                }
                var obj=data[0];
                dialog.loading.close();
                if(obj.error_code==0){
                    dialog.toast('查询成功，请去本地查看！', 'success', 1000);
                }else{
                    dialog.toast(obj.error_info, 'error', 1000);
                }
            }
        });
    });
    
});  
  /*****************本地信号质量*********************/
$("#queryLocalDCSign").on("click",function(){
    location.href=location.origin+"/diciMobile/views/localSignalList.html?nodeId="+nodeId;
});  
  /*****************本地系统参数*********************/
$("#queryLocalParam").on("click",function(){
    location.href=location.origin+"/diciMobile/views/localParamList.html?nodeId="+nodeId;
});  
  /*****************指令记录*********************/
$("#queryCmd").on("click",function(){
    location.href=location.origin+"/diciMobile/views/commandList.html?nodeId="+nodeId;
});  
  /*****************历史数据*********************/
$("#queryHistory").on("click",function(){
    location.href=location.origin+"/diciMobile/views/historyList.html?nodeId="+nodeId;
}); 
 
  /*****************查看标尺*********************/
$("#queryScale").on("click",function(){
    location.href=location.origin+"/diciMobile/views/staffList.html?nodeId="+nodeId;
}); 