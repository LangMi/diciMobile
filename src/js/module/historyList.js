var historyListJs={
    param:{
        "pageSize": '',
        "pageNo": '',
        "nodeId": "",
        "diciNo": ""
    },
    init:function(nodeId){
        this.param.nodeId=(nodeId!=undefined) ? nodeId : "";
        this.render("",this.param.parkName,"","");
    },
    render:function(pspId,parkName,nodeId,nowState){
        var that = this;
        $('#historyList').html('<div class="listcontent" id="historyListContent"></div>');
        var pageNo = 0, pageSize = 20;
        $("#YDUI_CONFRIM").remove();
        $('#historyList').infiniteScroll({
            pageSize: pageSize,
            initLoad: true,
            loadingHtml: '<strong>加载中...</strong>',
            doneTxt: '没有更多数据了',
            loadListFn: function () {
                var def = $.Deferred();
                var defaultsData = $.extend(true,historyListJs.param,{
                    "pageNo": pageNo,
                    "pageSize":  pageSize
                });         
                $.ajax({
                    type:"post",
                    url: interface.recvDataList,
                    dataType: 'json',
                    data: defaultsData,
                    xhrFields: {withCredentials: true},
                    success: function (data) {
                        if(data.code==500){
                            dialog.alert(data.msg,function(){
                                logout();
                            });
                        }
                        if(data.length!=0){
                            var listHtml = '';
                            for(var i=0;i<data.length;i++){
                                var obj=data[i];
                                console.log(obj, JSON.stringify(obj))
                                listHtml += '<div class="history-item">'+
                                    '<div class="cell-item" data-id="'+obj.id+'">'+
                                        '<div class="cell-left">'+
                                            '<div>'+
                                                '<div class="uptime"><i class="icon-arrow-up"></i>'+obj.eventTime+'</div>'+
                                                // '<div class="letter-sm">包序号：<span>'+obj.dataType+'</span></div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="cell-right cell-arrow">'+obj.typeValue+'</div>'+
                                        '<div class="desc" style="display: none">'+JSON.stringify(obj)+'</div>'+
                                    '</div>'+
                                '</div>';                             
                            }
                            $('#historyListContent').append(listHtml);
                            def.resolve(data);
                            ++pageNo;
                        }else{
                            $('.list-loading').hide();
                            $('#historyList').append('<div class="list-donetip">没有更多数据了</div>');
                        } 
                        that.itemClickFunc();                   
                    },
                    error:function(){
                        $('#historyList').html('<div class="list-donetip">未知错误</div>');
                    }
                });
                return def.promise();
            }
        });
        
    },
    itemClickFunc:function(){
        $("#historyListContent").on('click',".cell-item",function(){
            var id=$(this).data("id");
            if (id==""||id==null||id==undefined){
                dialog.toast('无', 'none', 1000);
                return;
            }
            var dataDesc = $(this).find('.desc').html();
            localStorage.setItem('historyListId', dataDesc);
            location.href=location.origin+"/diciMobile/views/historyDetail.html?nodeId="+id;
        });
    }
};