var paramJs={
    init:function(nodeId,type){
        this.render(nodeId,type);
        this.itemClickFunc(type);
    },
    render:function(nodeId,type){
        if(type=="edit"){
            $("input[name='heartBeat']").attr("disabled",false);
            var paramObj = $.parseJSON(localStorage.getItem('paramObj') || '{}');
            paramObj.nodeId=nodeId;
            $("input,select").each(function(){
                var param=$(this).attr("name");
                $(this).val(paramObj[param]);
            });
        }else{
            $("button").hide();
            var paramObj = JSON.parse(localStorage.getItem('paramItem').replace(/\'/g,"\""));
            paramObj.nodeId=nodeId;
            $("input,select").each(function(){
                var param=$(this).attr("name");
                $(this).val(paramObj[param]);
            });
        }
    },
    itemClickFunc:function(type){
        if(type=="edit"){
            $(".cell-item").on("click",function(){
                if($(this).find("select,input").attr("name")!="heartBeat"){
                    dialog.alert("只可修改心跳！");
                }
            });
            $("button").on("click",function(){
                if($("input[name='heartBeat']").val()==""){
                    dialog.alert("心跳值不能为空！");
                    return ;
                }
                dialog.confirm("",'确定要执行下发操作吗?', function () {
                    $.ajax({
                        type:"post",
                        url: interface.sendParam,
                        dataType: 'json',
                        xhrFields: {withCredentials: true},
                        data: {
                            diciNo:$("input[name='diciNo']").val(),
                            IMEI:$("input[name='nodeId']").val(),
                            heartBeat:$("input[name='heartBeat']").val(),
                            sendTime:$("input[name='sendTime']").val(),
                            magRef:$("input[name='magRef']").val(),
                            magRefWave:$("input[name='magRefWave']").val(),
                            abMagRef:$("input[name='abMagRef']").val(),
                            debugFlag:$("select[name='debugFlag']").val(),
                            nStableValue:$("input[name='nStableValue']").val(),
                            pStableValue:$("input[name='pStableValue']").val(),
                            ckInterval:$("input[name='ckInterval']").val(),
                            ckRound:$("input[name='ckRound']").val(),
                            enConf:$("select[name='enConf']").val(),
                            ckTime:$("input[name='ckTime']").val(),
                            hmcLevel:$("input[name='hmcLevel']").val(),
                            alEnable:$("input[name='alEnable']").val(),
                            abScaleEnable:$("input[name='abScaleEnable']").val(),
                            zShock:$("input[name='zShock']").val()
                        },
                        beforeSend:function(){
                            dialog.loading.open('执行中');
                        },
                        success: function (data) {
                            if(data.code==500){
                                dialog.alert(data.msg,function(){
                                    logout();
                                });
                            }
                            dialog.loading.close();
                            if(data.error_code==0){
                                dialog.toast('下发成功！', 'success', 1000);
                                history.back();
                            }else{
                                dialog.toast(data.error_info, 'error', 1000);
                            }
                        }
                    });
                });
                
            });
        }
    }
};