var paramListJs={
    param:{
        "pageSize": '',
        "pageNo": '',
        "nodeId": ""
    },
    init:function(nodeId){
        this.param.nodeId=(nodeId!=undefined) ? nodeId : "";
        this.render("paramList","paramListContent",0);
        this.render("paramListDown","paramListDownContent",1);
    },
    render:function(listid,listcontentid,dataType){
        $('#'+listid).html('<div class="listcontent" id="'+listcontentid+'"><div class="top" id="'+listid+'Top"></div></div>');
        var pageNo = 0, pageSize = 20;
        $('#'+listid).infiniteScroll({
            pageSize: pageSize,
            initLoad: true,
            binder:$("#"+listid),
            loadingHtml: '<strong>加载中...</strong>',
            doneTxt: '没有更多数据了',
            loadListFn: function () {
                var def = $.Deferred();
                var defaultsData = $.extend(true,paramListJs.param,{
                    // "pageNo": pageNo,
                    // "pageSize":  pageSize,
                    "dataType":dataType
                });               
                $.ajax({
                    type:"post",
                    url: interface.paramList,
                    dataType: 'json',
                    data: defaultsData,
                    xhrFields: {withCredentials: true},
                    success: function (data) {
                        if(data.code==500){
                            dialog.alert(data.msg,function(){
                                logout();
                            });
                        }
                        if(data.length!=0){
                            var listHtml = '';
                            for(var i=0;i<data.length;i++){
                                var obj=data[i];
                                var iconClass=(obj.dataType==1)?"icon-arrow-down":"icon-arrow-up";
                                listHtml += '<div class="history-item param-item" data-paramitem="'+JSON.stringify(obj).replace(/\"/g,"'")+'">'+
                                    '<div class="cell-item">'+
                                        '<div class="cell-left">'+
                                            '<div>'+
                                                '<div class="uptime"><i class="'+iconClass+'"></i>'+obj.createTime+'</div>'+
                                                '<div class="letter-sm">心跳：<span>'+obj.heartBeat+'</span></div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="cell-right cell-arrow">'+obj.dataTypeValue+'</div>'+
                                    '</div>'+
                                '</div>';           
                            }
                            $('#'+listcontentid).append(listHtml);
                            def.resolve(data);
                            ++pageNo;
                        }else{
                            $('.list-loading').hide();
                            $('#'+listid).append('<div class="list-donetip">没有更多数据了</div>');
                        }  
                        paramListJs.itemClickFunc();               
                    },
                    error:function(){
                        $('#'+listid).html('<div class="list-donetip">未知错误</div>');
                    }
                });
                return def.promise();
            }
        });
    },
    itemClickFunc:function(){
        var _this=this;
        $(document).on("click",".param-item",function(){
            localStorage.setItem('paramItem', $(this).data("paramitem"));
            location.href=location.origin+"/diciMobile/views/param.html?nodeId="+_this.param.nodeId+"&type=read";          
        });
    }
};