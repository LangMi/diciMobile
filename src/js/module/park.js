var parkJs={
    param:{
        name:""
    },
    init:function(){
        this.filter();
        this.render();
    },
    render:function(name){
        var listHeight=$('#parkList').height();
        $('#parkList').remove();
        $(".park-item").append('<div class="list" id="parkList" style="height:'+listHeight+'px">'+
            '<div class="listcontent" id="parkContent">'+
            '<div class="top" id="parktop"></div>'+
            '</div>'+
        '</div>');
        // $('#parkList').html('<div class="listcontent" id="parkContent"></div>');
        var pageNo = 0, pageSize = 20;
        $("#YDUI_CONFRIM .confirm-ft a").each(function(){
            if($(this).text()=="取消"){
                $(this).click();
            }
        });
        $('#parkList').infiniteScroll({
            pageSize: pageSize,
            initLoad: true,
            binder:$("#parkList"),
            loadingHtml: '<strong>加载中...</strong>',
            loadListFn: function () {
                var def = $.Deferred();
                $.ajax({
                    type:"post",
                    url: interface.parkList,
                    dataType: 'json',
                    xhrFields: {withCredentials: true},
                    data: { pageNo: pageNo, pageSize: pageSize,name:name},
                    success: function (data) {
                        if(data.code==500){
                            dialog.alert(data.msg,function(){
                                logout();
                            });
                        }
                        if(data.length!=0){
                            for(var i=0;i<data.length;i++){
                                var obj=data[i];
                                console.log(obj)
                                var html = '<div class="cell-item" data-name="'+obj.name+'"><div class="cell-left">'+obj.name+'</div><div class="cell-right cell-arrow"><span class="badge badge-primary">'+obj.spNum+'</span></div></div>';
                                $('#parkContent').append(html);
                            }
                            def.resolve(data);
                            ++pageNo;
                        }else{
                            $('.list-loading').hide();
                            $('#parkList').append('<div class="list-donetip">没有更多数据了</div>');
                        }  
                    },
                    error:function(){
                        $('#parkList').html('<div class="list-donetip">未知错误</div>');
                    }
                });
                return def.promise();
            }
        });
        this.itemClickFunc();
    },
    itemClickFunc:function(){
        $("#parkContent").on('click',".cell-item",function(){
            var parkName=$(this).data("name");
            location.href=location.origin+"/diciMobile/views/parkunitList.html?parkName="+parkName;
        });
    },
    filter:function(){
        var _this=this;
        $("#choosePark").on("click",function(){
            dialog.confirm('停车场筛选', $("#dialogContent").find(".parkDialog").html().replace(/"parkName"/,'"parkName"'+' value='+_this.param.name+' '), [
                {
                    txt: '取消',
                    color: false, /* false:黑色  true:绿色 或 使用颜色值 */
                    callback: function () {
                    }
                },
                {
                    txt: '重置',
                    stay: true, /* 是否保留提示框 */
                    color: false, /* 使用颜色值 */
                    callback: function () {
                        $("#YDUI_CONFRIM").find("input").val("");
                    }
                },
                {
                    txt: '确定',
                    color: true,
                    stay: true,
                    callback: function () {
                        var name = $("#YDUI_CONFRIM").find("input[name='parkName']").val();
                        _this.param.name=name;
                        _this.render(name);
                    }
                }
            ]);
        });
    }
};