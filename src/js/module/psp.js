var pspJs={
    param:{
        pspId:"",
        parkName:"",
        nodeId:"",
        nowState:""
    },
    init:function(name){
        this.param.parkName=(name!=undefined) ? name : "";
        this.filter(name);
        this.render("",this.param.parkName,"","");
    },
    render:function(pspId,parkName,nodeId,nowState){
        var listHeight=$('#pspList').height();
        $('#pspList').remove();
        $(".psp-item").append('<div class="list" id="pspList" style="height:'+listHeight+'px">'+
            '<div class="listcontent" id="pspContent">'+
            '<div class="top" id="psptop"></div>'+
            '</div>'+
        '</div>');
        var pageNo = 0, pageSize = 20;
        $("#YDUI_CONFRIM .confirm-ft a").each(function(){
            if($(this).text()=="取消"){
                $(this).click();
            }
        });
        $('#pspList').infiniteScroll({
            pageSize: pageSize,
            initLoad: true,
            binder:$("#pspList"),
            loadingHtml: '<strong>加载中...</strong>',
            loadListFn: function () {
                var def = $.Deferred();
                $.ajax({
                    type:"post",
                    url: interface.pspList,
                    dataType: 'json',
                    xhrFields: {withCredentials: true},
                    data: { pageNo: pageNo, pageSize: pageSize,parkName:parkName,pspId:pspId,nodeId:nodeId,nowState:nowState},
                    success: function (data) {
                        if(data.code==500){
                            dialog.alert(data.msg,function(){
                                logout();
                            });
                        }
                        if(data.length!=0){
                            for(var i=0;i<data.length;i++){
                                var obj=data[i];
                                var circle=(obj.nowState==1)? "circle-orange":"circle-green";
                                var forbid=(obj.nodeId=="")? "forbid":"";
                                var isOnline=(obj.onlineFlag==true)? "在线":"离线";
                                var parkname=(obj.parkName!=null&&obj.parkName!="")? obj.parkName:"无停车场";
                                var html = '<div class="cell-item '+forbid+'" data-nodeid="'+obj.nodeId+'"><div class="cell-left"><span>['+isOnline+']</span>'+obj.pspId+'<span class="addr">/'+parkname+'</span></div><div class="cell-right cell-arrow"><span class="'+circle+'"></span></div></div>';
                                $('#pspContent').append(html);
                            }
                            def.resolve(data);
                            ++pageNo;
                        }else{
                            $('.list-loading').hide();
                            $('#pspList').append('<div class="list-donetip">没有更多数据了</div>');
                        } 
                        
                    },
                    error:function(){
                        $('#pspList').html('<div class="list-donetip">未知错误</div>');
                    }
                });
                return def.promise();
            }
        });
        this.itemClickFunc();
    },
    itemClickFunc:function(){
        $("#pspContent").on('click',".cell-item",function(){
            var nodeId=$(this).data("nodeid");
            if (nodeId==""||nodeId==null||nodeId==undefined){
                dialog.toast('没有地磁！', 'none', 1000);
            }else{
                location.href=location.origin+"/diciMobile/views/diciDetail.html?nodeId="+nodeId;
            }
        });
    },
    filter:function(name){
        var _this=this;
        // console.log(_this.param)
        $("#choosePsp").on("click",function(){
            console.log(_this.param)
            dialog.confirm('停车位筛选', $("#dialogContent").find(".pspDialog").html()
            .replace(/"pspId"/,'"pspId"'+' value='+_this.param.pspId+' ')
            .replace(/"parkName"/,'"parkName"'+' value='+_this.param.parkName+' ')
            .replace(/"nodeId"/,'"nodeId"'+' value='+_this.param.nodeId+' ')
            .replace(/{"nowState" value="0|1" selected}/,"$1+=false")
            .replace('"nowState" value="'+_this.param.nowState+'"','"nowState" value="'+_this.param.nowState+'" selected'), [
                {
                    txt: '取消',
                    color: false, /* false:黑色  true:绿色 或 使用颜色值 */
                    callback: function () {
                    }
                },
                {
                    txt: '重置',
                    stay: true, /* 是否保留提示框 */
                    color: false, /* 使用颜色值 */
                    callback: function () {
                        $("#YDUI_CONFRIM").find("input").val("");
                        $("#YDUI_CONFRIM").find("select").val("");
                        $("#YDUI_CONFRIM").find("input[name='parkName']").val((name!=undefined) ? name : "");                        
                    }
                },
                {
                    txt: '确定',
                    color: true,
                    stay: true,
                    callback: function () {
                        var pspId = $("#YDUI_CONFRIM").find("input[name='pspId']").val();
                        var parkName = $("#YDUI_CONFRIM").find("input[name='parkName']").val();
                        var nodeId = $("#YDUI_CONFRIM").find("input[name='nodeId']").val();
                        var nowState = $("#YDUI_CONFRIM").find("select[name='nowState']").val();
                        _this.param.pspId=pspId;
                        _this.param.parkName=parkName;
                        _this.param.nodeId=nodeId;
                        _this.param.nowState=nowState;
                        _this.render(pspId,parkName,nodeId,nowState);
                    }
                }
            ]);
        });
    }
};