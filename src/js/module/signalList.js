var signalListJs={
    param:{
        "pageSize": '',
        "pageNo": '',
        "nodeId": ""
    },
    init:function(nodeId){
        this.param.nodeId=(nodeId!=undefined) ? nodeId : "";
        this.render("",this.param.parkName,"","");
    },
    render:function(pspId,parkName,nodeId,nowState){
        $('#signalList').html('<div class="listcontent" id="signalListContent"></div>');
        var pageNo = 0, pageSize = 20;
        $("#YDUI_CONFRIM").remove();
        $('#signalList').infiniteScroll({
            pageSize: pageSize,
            initLoad: true,
            loadingHtml: '<strong>加载中...</strong>',
            doneTxt: '没有更多数据了',
            loadListFn: function () {
                var def = $.Deferred();
                var defaultsData = $.extend(true,signalListJs.param,{
                    "pageNo": pageNo,
                    "pageSize":  pageSize
                });               
                $.ajax({
                    type:"post",
                    url: interface.signalList,
                    dataType: 'json',
                    data: defaultsData,
                    xhrFields: {withCredentials: true},
                    success: function (data) {
                        if(data.code==500){
                            dialog.alert(data.msg,function(){
                                logout();
                            });
                        }
                        if(data.length!=0){
                            var listHtml = '';
                            for(var i=0;i<data.length;i++){
                                var obj=data[i];
                                listHtml += '<div class="history-item">'+
                                    '<div class="cell-item">'+
                                        '<div class="cell-left">'+
                                            '<div>'+
                                                '<div class="uptime">'+obj.eventTime+'</div>'+
                                                '<div class="letter-sm">IMEI号：<span>'+obj.nodeId+'</span></div>'+
                                                '<div class="letter-sm">信号强度：<span>'+obj.signPower+'</span></div>'+
                                                '<div class="letter-sm">信号覆盖范围：<span>'+obj.signScope+'</span></div>'+
                                                '<div class="letter-sm">信噪比：<span>'+obj.snr+'</span></div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="cell-right"></div>'+
                                    '</div>'+
                                '</div>';                         
                            }
                            $('#signalListContent').append(listHtml);
                            def.resolve(data);
                            ++pageNo;
                        }else{
                            $('.list-loading').hide();
                            $('#signalList').append('<div class="list-donetip">没有更多数据了</div>');
                        }                     
                    },
                    error:function(){
                        $('#signalList').html('<div class="list-donetip">未知错误</div>');
                    }
                });
                return def.promise();
            }
        });
    }
};