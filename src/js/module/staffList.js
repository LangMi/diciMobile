var staffListJs={
    param:{
        // "pageSize": '',
        // "pageNo": '',
        "nodeId": ""
    },
    init:function(nodeId){
        this.param.nodeId=(nodeId!=undefined) ? nodeId : "";
        this.render("",this.param.parkName,"","");
    },
    render:function(pspId,parkName,nodeId,nowState){
        $('#staffList').html('<div class="listcontent" id="staffListContent"></div>');
        var pageNo = 0, pageSize = 20;
        $("#YDUI_CONFRIM").remove();
        $('#staffList').infiniteScroll({
            pageSize: pageSize,
            initLoad: true,
            loadingHtml: '<strong>加载中...</strong>',
            doneTxt: '没有更多数据了',
            loadListFn: function () {
                var def = $.Deferred();
                var defaultsData = $.extend(true,staffListJs.param,{
                    // "pageNo": pageNo,
                    // "pageSize":  pageSize
                });               
                $.ajax({
                    type:"post",
                    url: interface.staffList,
                    dataType: 'json',
                    data: defaultsData,
                    xhrFields: {withCredentials: true},
                    success: function (data) {
                        console.log(data)
                        if(data.code==500){
                            dialog.alert(data.msg,function(){
                                logout();
                            });
                        }
                        if(data.length!=0){
                            var listHtml = '';
                            for(var i=0;i<data.length;i++){
                                var obj=data[i];
                                console.log((obj.varX==null)?"":obj.varX)
                                var varposition='('+((obj.varX==null)?" ":obj.varX)+','+((obj.varY==null)?" ":obj.varY)+','+((obj.varZ==null)?" ":obj.varZ)+')';
                                var axisposition='('+((obj.axisX==null)?" ":obj.axisX)+','+((obj.axisY==null)?" ":obj.axisY)+','+((obj.axisZ==null)?" ":obj.axisZ)+')';
                                listHtml += '<div class="history-item">'+
                                    '<div class="cell-item">'+
                                        '<div class="cell-left">'+
                                            '<div>'+
                                                '<div class="uptime"><i class="icon-arrow-up"></i>'+obj.eventTime+'</div>'+
                                                // '<div class="letter-sm">包序号：<span>'+obj.dataType+'</span></div>'+
                                                // '<div class="letter-sm">变化量：<span>'+varposition+'</span></div>'+
                                                '<div class="letter-sm">标尺：<span>'+axisposition+'</span></div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="cell-right">'+obj.typeValue+'</div>'+
                                    '</div>'+
                                '</div>';                         
                            }
                            $('#staffListContent').append(listHtml);
                            def.resolve(data);
                            ++pageNo;
                        }else{
                            $('.list-loading').hide();
                            $('#staffList').append('<div class="list-donetip">没有更多数据了</div>');
                        }                     
                    },
                    error:function(){
                        $('#staffList').html('<div class="list-donetip">未知错误</div>');
                    }
                });
                return def.promise();
            }
        });
    }
};